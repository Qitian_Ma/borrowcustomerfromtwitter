import os
__dir__ = "/home/multifaceted/Documents/Supermercato24"
os.chdir(__dir__)

from Auth import Auths
import pandas as pd
import numpy as np
import tweepy
import time

tweepy.__file__
tweepy.__version__
tweepy.__author__
auths = Auths("Real_Keys.txt")
auth = auths.fetch_next()
api = tweepy.API(auth)

# get company info
companys = ["Coopitalia", "CarrefourItalia", "LidlItalia", "Conad", "supermercato24"]
companys_info = []

for company in companys:
    companys_info.append(api.get_user(company))
del company

companys_info_df = pd.DataFrame()

attrs_ls = ["id",
            "name",
            "screen_name",
            "location",
            "profile_location",
            "description",
            "url",
            "entities['description']",
            "entities['url']",
            "protected",
            "followers_count",
            "friends_count",
            "listed_count",
            "created_at",
            "favourites_count",
            "utc_offset",
            "time_zone",
            "geo_enabled",
            "verified",
            "statuses_count",
            "lang"]

dict_value = ["company_info." + attr + " if hasattr(company_info, \"" + attr + "\") else None" for attr in attrs_ls]

for company_info in companys_info:
    companys_info_df = companys_info_df.append({key:eval(value) for (key, value) in np.swapaxes([attrs_ls, dict_value], 0 ,1)}, ignore_index=True)
del company_info
del dict_value

companys_info_df.to_csv("companys.csv")

# Take Carrefour as an example.
COMPANY_NAME = "CarrefourItalia"

# get followers info
followers_info_df = pd.DataFrame()
dict_value = ["follower_info." + attr + " if hasattr(follower_info, \"" + attr + "\") else None" for attr in attrs_ls]
cursor = -1
while True:
    try:
        page = api.followers(COMPANY_NAME, cursor=cursor)
        if len(page[0]) == 0: break
        for follower_info in page[0]:
            followers_info_df = followers_info_df.append({key:eval(value) for (key, value) in np.swapaxes([attrs_ls, dict_value], 0 ,1)}, ignore_index=True)
        del follower_info
        cursor = page[-1][1]
    except tweepy.RateLimitError:
        followers_info_df.to_csv(COMPANY_NAME + "_followers.csv")
        auths.freeze()
        auth = auths.fetch_next()
        if isinstance(auth, int):
            print("Rate limit exceeded. Please wait", auth, "seconds.")
            time.sleep(auth)
            auth = auths.fetch_next()
        api = tweepy.API(auth)
del cursor
del dict_value

# create friendship
api.create_friendship("Coopitalia")
# send direcct messages
api.send_direct_message(recipient_id="1174604729005039616", text="hello")
