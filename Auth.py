class Auths:
    """
    Auths is a class that helps rotate twitter keys.
    """

    def __init__(self, directory):
        from datetime import datetime
        import numpy as np
        import tweepy
        import json

        # Waiting time when a key has reached the rate limit.
        self.WAITING_TIME = 15 * 60

        # Auths is a list of list. For the inner list, the first element is a key, the second element is the time when it reached the time limit.
        # Actually you can view auths as a ring of keys.
        self.auths = []

        # Read a list of keys.
        auth_keys = json.load(open(directory))

        # Drop duplicated keys.
        auth_keys = np.unique(auth_keys, axis = 0)

        for consumer_key, consumer_secret, access_key, access_secret in auth_keys:
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_key, access_secret)
            time_now = int(datetime.timestamp(datetime.now()))
            self.auths.append([auth, time_now - self.WAITING_TIME - 1])

        self.total = len(auth_keys)

        # Index points to the current key position.
        self.index = self.total - 1

        if self.index == -1:
            print("*** WARNING: No keys found! ***")

    def fetch_next(self):
        """
        Fetch next key.
        """

        from datetime import datetime

        self.index = (self.index + 1) % self.total
        time_now = int(datetime.timestamp(datetime.now()))
        time_delta = time_now - self.auths[self.index][1]

        # If the time since the key reached the rate limit is more than the waiting time, the key is available.
        if time_delta > self.WAITING_TIME:
            print("*** Switch to key", self.index, ". ***")
            return self.auths[self.index][0]

        # Else the key is not available. In this case, return the remaining time before the key is available.
        else:
            print("*** Key", self.index, "is frozen. ***")
            self.index = (self.index - 1) % self.total
            return self.WAITING_TIME - time_delta + 1

    def fetch_current(self):
        """
        Fetch current key.
        """

        return self.auths[self.index][0]

    def freeze(self):
        """
        Freeze current key.
        Since the object itself doesn't know if the key is available, this method must be called outside of the object.
        """

        from datetime import datetime

        time_now = int(datetime.timestamp(datetime.now()))
        self.auths[self.index][1] = time_now
